new Vue({
    el: '#vue-app',
    data: {
        age: 21,
    },
    methods: {
        logName: function(){
            console.log('You entered your name.');
        },
        logAge: function(){
            console.log('You entered your age.');
        }
    }
})